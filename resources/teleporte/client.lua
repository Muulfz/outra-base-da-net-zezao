local interiores = {
}

local Loterica = {
	{nome = "Hospital", id = 80, cor = 1, x = -449.92349243164,y = -340.94372558594,z = 34.501441955566},
}

Citizen.CreateThread(function()
	for _, item in pairs(Loterica) do
      	item.blip = AddBlipForCoord(item.x, item.y, item.z)
      	SetBlipSprite(item.blip, item.id)
      	SetBlipColour(item.blip, item.cor)
      	SetBlipAsShortRange(item.blip, true)
      	BeginTextCommandSetBlipName("STRING")
      	AddTextComponentString(item.nome)
      	EndTextCommandSetBlipName(item.blip)
    end
end)

Entrar = {
	x = 1151.0792236328,y = -1529.9782714844,z = 35.373233795166 -- Hospital Dentro
}

Sair = {
	x = 251.61322021484, y = -1366.37890625, z = 39.534332275391 -- Hospital Fora
}

distancia = 50.5999 -- distancia em que o drawmarker já fica visivel
contador = 0 -- Tempinho para aparecer dentro
interior_atual = 0

-- Envia um pedido ao server pedindo os dados
AddEventHandler("playerSpawned", function()
  TriggerServerEvent("interiores:enviar_dados")
end)

-- Manter isto ativado caso fique reiniciando pelo F8, quando não precisar mais comentar este evento. !!! NÃO ESQUECER !!!
TriggerServerEvent("interiores:enviar_dados")

-- Recebe os dados do servidor
RegisterNetEvent("interiores:receber_dados")
AddEventHandler("interiores:receber_dados", function(stunner)
  interiores = stunner -- Se foder vai
end)

-- Aquele Draw Text maroto de lei
function DrawAdvancedText(x,y ,w,h,scale, text, r,g,b,a)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
	DrawText(x - 0.2+w, y - 0.02+h)
end

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(1)
		local Coordenadas = GetEntityCoords(GetPlayerPed(-1))
		local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, Entrar.x, Entrar.y, Entrar.z, true)
		if Distancia < 18.0 then
			Opacidade = math.floor(255 - (Distancia * 12))
			Texto3D(Entrar.x, Entrar.y, Entrar.z, "PRESSIONE ~y~[ E ] ~w~PARA ENTRAR", Opacidade)
		end
	end
end)

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(1)
		local Coordenadas = GetEntityCoords(GetPlayerPed(-1))
		local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, Sair.x, Sair.y, Sair.z, true)
		if Distancia < 15.0 then
			Opacidade = math.floor(255 - (Distancia * 40))
			Texto3D(Sair.x, Sair.y, Sair.z, "PRESSIONE ~y~[ E ] ~w~PARA SAIR", Opacidade)
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if contador > 0 and interior_atual > 0 then DrawAdvancedText(1.030, 0.973, 0.001, 0.0018, 0.80, "~s~"..interiores[interior_atual].nome, 255, 255, 255, 255, 1, 1) end
			for i = 1, #interiores do
				if not IsEntityDead(PlayerPedId()) then
					if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), interiores[i].x_dentro,interiores[i].y_dentro,interiores[i].z_dentro, true) < distancia then
						DrawMarker(27, interiores[i].x_dentro,interiores[i].y_dentro,interiores[i].z_dentro-0.98, 0, 0, 0, 0, 0, 0,1.3001,1.3001,1.5001, 255, 255, 255, 150, 0, 0, 2, 0, 0, 0, 0)
						if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), interiores[i].x_dentro,interiores[i].y_dentro,interiores[i].z_dentro, true) < 1.599 and (IsControlJustReleased(1, 51)) then
							if contador == 0 then
								DoScreenFadeOut(1000)
								while IsScreenFadingOut() do Citizen.Wait(0) end
								NetworkFadeOutEntity(GetPlayerPed(-1), true, false)
								Wait(1000)
								SetEntityCoords(GetPlayerPed(-1), interiores[i].x_fora,interiores[i].y_fora,interiores[i].z_fora)
								NetworkFadeInEntity(GetPlayerPed(-1), 0)
								Wait(1000)
								interior_atual = i
								contador = 5
								SimulatePlayerInputGait(PlayerId(), 1.0, 100, 1.0, 1, 0)
								DoScreenFadeIn(1000)
								while IsScreenFadingIn() do Citizen.Wait(0)	end
							end
						end
					end
					if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), interiores[i].x_fora,interiores[i].y_fora,interiores[i].z_fora, true) < distancia then
						DrawMarker(27,interiores[i].x_fora,interiores[i].y_fora,interiores[i].z_fora-0.98, 0, 0, 0, 0, 0, 0,1.3001,1.3001,1.5001, 255, 255, 255, 155, 0, 0, 2, 0, 0, 0, 0)
						if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), interiores[i].x_fora,interiores[i].y_fora,interiores[i].z_fora, true) < 1.599 and (IsControlJustReleased(1, 51)) then
							if contador == 0 then
								DoScreenFadeOut(1000)
								while IsScreenFadingOut() do Citizen.Wait(0) end
								NetworkFadeOutEntity(GetPlayerPed(-1), true, false)
								Wait(1000)
								SetEntityCoords(GetPlayerPed(-1), interiores[i].x_dentro,interiores[i].y_dentro,interiores[i].z_dentro)
								NetworkFadeInEntity(GetPlayerPed(-1), 0)
								Wait(1000)
								interior_atual = i
								contador = 5
								SimulatePlayerInputGait(PlayerId(), 1.0, 100, 1.0, 1, 0)
								DoScreenFadeIn(1000)
								while IsScreenFadingIn() do Citizen.Wait(0)	end
							end
						end
					end
				end
			end
	--  end | Serve pra nada esse END, mas se tirar vai quebrar a escadinha, ai fica feio men
	end
end)

-- Faz aquela contagem regressiva marota
Citizen.CreateThread(function()
	while true do
		Wait(1000)
		if contador > 0 then
			contador=contador-1
			if contador == 0 then interior_atual = 0 end
		end
	end
end)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TEXTO 3D
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Texto3D(x,y,z, text, Opacidade)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())    
    if onScreen then
        SetTextScale(0.6, 0.6)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, Opacidade)
        SetTextDropshadow(0, 0, 0, 0, Opacidade)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end