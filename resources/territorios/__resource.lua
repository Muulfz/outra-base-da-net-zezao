dependency "vrp"

client_scripts{ 
	"@vrp/lib/utils.lua",
  	"cfg/favela.lua",
  	"client.lua"
}

server_scripts{ 
  	"@vrp/lib/utils.lua",
  	"cfg/favela.lua",
  	"server.lua"
}
