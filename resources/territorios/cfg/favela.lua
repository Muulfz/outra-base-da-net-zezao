cfg = {}

cfg.seconds = 500

cfg.favelas = { 
	["Favela1"] = {
		position = { ['x'] = 771.35, ['y'] = -233.64, ['z'] = 66.21 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Capão Redondo",
		lastdommed = 0
	},
	["Favela2"] = {
		position = { ['x'] = 1368.8739013672, ['y'] = -580.14691162109, ['z'] = 74.380256652832 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Heliópolis",
		lastdommed = 0
	},
	["Favela3"] = {
		position = { ['x'] = 425.54315185547, ['y'] = -1783.0893554688, ['z'] = 28.784479141235 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Brasilandia",
		lastdommed = 0
	},
	["Favela4"] = {
		position = { ['x'] = 1395.4786376953, ['y'] = -1578.9244384766, ['z'] = 56.882274627686 },
		reward = 0 + math.random(0,0),
		nameoffavela = "Paraísopolis",
		lastdommed = 0	
	}
}