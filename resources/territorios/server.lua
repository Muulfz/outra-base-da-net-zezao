local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")

local favelas = cfg.favelas
local contador = 0
local dommers = {}

local drogas = { 
	"Maconha",
	"LSD",
	"cocaina"
}

function get3DDistance(x1, y1, z1, x2, y2, z2)
	return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2) + math.pow(z1 - z2, 2))
end

RegisterServerEvent('es_favela:toofar')
AddEventHandler('es_favela:toofar', function(domm)
	if(dommers[source])then
		TriggerClientEvent('es_favela:toofarlocal', source)
		dommers[source] = nil
		local user_id = vRP.getUserId(source)
		local faccao = vRP.getUserGroupByType(user_id,"job")
		TriggerClientEvent('chatMessage', -1, 'TWITTER', { 0, 0x99, 255 }, "A facção ^1" .. faccao .. " ^7fugiu da invasão em ^1" .. favelas[domm].nameoffavela .. " ^7, devido a isso a invasão foi cancelada.")
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Você fugiu do território",
			type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	end
end)

RegisterServerEvent('es_favela:playerdied')
AddEventHandler('es_favela:playerdied', function(domm)
	if(dommers[source])then
		TriggerClientEvent('es_favela:playerdiedlocal', source)
		dommers[source] = nil
		TriggerClientEvent('chatMessage', -1, 'TWITTER', { 0, 0x99, 255 }, identity.name .. " "  .. identity.firstname .. "morreu enquanto tentava dominar ^2" .. favelas[domm].nameoffavela .. ", ^7a invasão foi cancelada.")
	end
end)

RegisterServerEvent('es_favela:dom')
AddEventHandler('es_favela:dom', function(domm)
  	local user_id = vRP.getUserId(source)
  	local player = vRP.getUserSource(user_id)
  	local bandidos = vRP.getUsersByPermission("invadir.favela")
  	if vRP.hasPermission(user_id,"invadir.favela") then
  		if #bandidos >= 1 then
	  		if contador == 0 then
			    if favelas[domm] then
					local favela = favelas[domm]
					local faccao = vRP.getUserGroupByType(user_id,"job")
					TriggerClientEvent('chatMessage', -1, 'TWITTER', { 0, 0x99, 255 }, "^1" .. faccao .. " ^7iniciou uma invasão em ^1" .. favela.nameoffavela)
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Voce começou uma invasão em " .. favela.nameoffavela .. ", não vai muito pra longe não em!",
						type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
					Citizen.Wait(1000)
					TriggerClientEvent("pNotify:SendNotification", player, {
						text = "Espere 15 minutos e você terá dominado completamente",
						type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
					TriggerClientEvent('es_favela:currentlydomming', player, domm)
					favelas[domm].lastdommed = os.time()
					dommers[player] = domm
					contador = 259200
					local savedSource = player
					SetTimeout(1000*60*15, function()
						if (dommers[savedSource]) then
							local pagamento = math.random(1000,5000)
							vRP.giveMoney(player,pagamento)
							local quantidade = math.random(10,50)
							local droga = drogas[math.random(1,#drogas)]
							vRP.giveInventoryItem(player,droga,quantidade,true)
							TriggerClientEvent('chatMessage', -1, 'SISTEMA', { 0, 0x99, 255 }, "Você recebeu ^1" .. pagamento .. " ^7reais e ^1" .. quantidade .. " " .. droga .. " ^7por dominar ^1" .. favela.nameoffavela)
							TriggerClientEvent('chatMessage', -1, 'TWITTER', { 0, 0x99, 255 }, "^1" .. faccao .. "^7 conseguiu dominar ^1" .. favela.nameoffavela)
							TriggerClientEvent('es_favela:dommerycomplete', savedSource, favela.reward)
						end
					end)
				end
	  		else
	  			dias = string.format("%02.f", math.floor(contador/3600/24));
				contagem = string.format("%02.f", math.floor(contador/3600));
				horas = string.format("%02.f", math.floor(contador/3600/3));
				minutos = string.format("%02.f", math.floor(contador/60 - (contagem*60)));
				segundos = string.format("%02.f", math.floor(contador - contagem*3600 - minutos *60))    

	  			TriggerClientEvent('chatMessage', player, 'Sistema', { 0, 0x99, 255 }, "Aguarde " .. string.format("%.0f", dias) .. " dias ".. string.format("%.0f", horas) .. " horas " .. string.format("%.0f", minutos) .. " minutos e " .. string.format("%.0f", segundos) .. " segundos para dominar novamente")
			end
  		else
			TriggerClientEvent('chatMessage', player, 'Sistema', { 0, 0x99, 255 }, "É necessário no minimo ^15 ^7bandidos para tentar dominar este território.")
  		end
  	else
  		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Teu lugar não é aqui, VAZA!",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
  	end
end)

Citizen.CreateThread(function()
	while true do
		Wait(1000)
		if contador > 0 then
			contador=contador-1
		end
	end
end)