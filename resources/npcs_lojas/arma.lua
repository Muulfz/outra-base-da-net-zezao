local barberPeds = {
	{model= "s_m_y_AmmuCity_01", voice="S_M_Y_AMMUCITY_01", x=22.288034439087, y=-1105.375, z=29.797004699707},
    {model= "s_m_y_AmmuCity_01", voice="S_M_Y_AMMUCITY_01", x=842.03045654297, y=-1035.3469238281, z=28.19486618042},
    {model= "s_m_y_AmmuCity_01", voice="S_M_Y_AMMUCITY_01", x=809.88238525391, y=-2159.0815429688, z=29.61901473999},
    {model= "s_m_y_AmmuCity_01", voice="S_M_Y_AMMUCITY_01", x=253.8409576416, y=-50.65661239624, z=69.941055297852},
    {model= "s_m_y_AmmuCity_01", voice="S_M_Y_AMMUCITY_01", x=-1118.9826660156, y=2699.7009277344, z=18.554149627686},
    {model= "s_m_y_AmmuCity_01", voice="S_M_Y_AMMUCITY_01", x=-1304.1818847656, y=-394.70712280273, z=36.695774078369},
    {model= "s_m_y_AmmuCity_01", voice="S_M_Y_AMMUCITY_01", x=-662.29235839844, y=-933.58935546875, z=21.829233169556}
}

Citizen.CreateThread(function() AmmuCity01SMY
	for k,v in ipairs(barberPeds) do
		RequestModel(GetHashKey(v.model))
		while not HasModelLoaded(GetHashKey(v.model)) do
			Wait(0)
		end

		barber = CreatePed(4, GetHashKey(v.model), v.x, v.y, v.z, v.h, false, false)
		SetBlockingOfNonTemporaryEvents(barber, true)
		SetAmbientVoiceName(barber, v.voice)
		TaskStartScenarioInPlace(barber, "WORLD_HUMAN_STAND_IMPATIENT_UPRIGHT", 0, 0)

		SetModelAsNoLongerNeeded(GetHashKey(v.model))
	end
end)
