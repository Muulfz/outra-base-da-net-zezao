-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- DESENVOLVIDO POR: SIGHMIR E SHADOW
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIÁVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
LTserver = Tunnel.getInterface("loteria")

local menuEnabled = false
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BLIPS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Loterica = {
	{nome = "Lotérica", id = 431, cor = 46, x = 926.62, y = 45.23, z = 80.90},
}

Citizen.CreateThread(function()
	for _, item in pairs(Loterica) do
      	item.blip = AddBlipForCoord(item.x, item.y, item.z)
      	SetBlipSprite(item.blip, item.id)
      	SetBlipColour(item.blip, item.cor)
      	SetBlipAsShortRange(item.blip, true)
      	BeginTextCommandSetBlipName("STRING")
      	AddTextComponentString(item.nome)
      	EndTextCommandSetBlipName(item.blip)
    end
end)

RegisterNetEvent("ToggleActionmenu")
AddEventHandler("ToggleActionmenu", function()
	ToggleActionMenu()
end)

function ToggleActionMenu()
	menuEnabled = not menuEnabled
	if (menuEnabled) then
		SetNuiFocus(true, true)
		SendNUIMessage({
			showPlayerMenu = true
		})
	else
		SetNuiFocus( false )
		SendNUIMessage({
			showPlayerMenu = false
		})
	end
end

function killTutorialMenu()
	SetNuiFocus(false)
	SendNUIMessage({
		showPlayerMenu = false
	})
	menuEnabled = false
end

RegisterNUICallback('close', function(data, cb)
	ToggleActionMenu()
	cb('ok')
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THREAD PRINCIPAL
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lotericas = {
	x = 925.97, y = 46.32, z = 80.90
}

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(1)
		local Coordenadas = GetEntityCoords(GetPlayerPed(-1))
		local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, Lotericas.x, Lotericas.y, Lotericas.z, true)
		if Distancia < 5.0 then
			Opacidade = math.floor(255 - (Distancia * 40))
			Texto3D(925.97,46.32,80.90, "PRESSIONE ~y~[ E ] ~w~PARA ACESSAR A LOTÉRICA", Opacidade)
			DrawMarker(27, 925.97,46.32,79.90, 0, 0, 0, 0, 0, 0, 1.501, 1.5001, 0.5001, 255, 255, 255, Opacidade, 0, 0, 0, 0)
	 		if (IsControlJustReleased(1, 51)) then
				SetNuiFocus( true, true )
				SendNUIMessage({
					showPlayerMenu = true
				})
		 	end
		end
	end
end)

RegisterNUICallback('buyTickets', function(data, cb)
	LTserver.buyTickets(data.amount)
	cb('ok')
end)

RegisterNUICallback('closeButton', function(data, cb)
	killTutorialMenu()
	cb('ok')
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TEXTO 3D
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Texto3D(x,y,z, text, Opacidade)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())    
    if onScreen then
        SetTextScale(0.6, 0.6)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, Opacidade)
        SetTextDropshadow(0, 0, 0, 0, Opacidade)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end