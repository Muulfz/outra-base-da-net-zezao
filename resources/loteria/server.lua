-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- DESENVOLVIDO POR: SIGHMIR E SHADOW
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIÁVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
vRPlt = {}
Tunnel.bindInterface("loteria",vRPlt)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUNÇÃO PARA COMPRAR TICKET
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function vRPlt.buyTickets(amount)
   local user_id = vRP.getUserId(source) .. ""
   local player = vRP.getUserSource(tonumber(user_id))
   local tickets = json.decode(vRP.getSData("lotto:tickets"))
   if not tickets then tickets = {} end
   if not tickets[user_id] then tickets[user_id] = {} end
   if ((#tickets[user_id])+amount) <= 5 then -- 5 é o limite de tickets
      if vRP.tryPayment(tonumber(user_id),100*amount) then
         local money = json.decode(vRP.getSData("lotto:money"))
         TriggerClientEvent("pNotify:SendNotification", player, {
            text = "Você comprou: " .. amount .. " ticket(s)",
            type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
         })
         if not money then money = 0 end
         vRP.setSData("lotto:money",json.encode(money+(100*amount)))
         for i=1,amount do 
            table.insert(tickets[user_id], math.random(1000,9999))
         end
         vRP.setSData("lotto:tickets",json.encode(tickets))
    else
      TriggerClientEvent('chatMessage', player, 'Loteria', { 255, 0, 0 }, "Você não possui dinheiro suficiente!")
      end
   else
    TriggerClientEvent('chatMessage', player, 'Loteria', { 255, 0, 0 }, "Você só pode comprar 5 tickets a cada rodada")
  end
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUNÇÃO TICKET DA LOTERIA
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function tick_lotto()
   local old_time = json.decode(vRP.getSData("lotto:time"))
   if not old_time then old_time = 0 end
   local randomMsg = {	
      "Faltam apenas " .. math.ceil(30-((os.time() - old_time )/60)) .." minutos para ser realizado o sorteio da loteria",
     	"Não se esqueça de passar na lotérica para comprar o próximo ticket premiado",
	}

   if (os.time() - old_time > (30*60)) then -- checa se passou 30 vezes 60 segundos, 30 minutos
      local no_winner = true
      local winner = math.random(1000,9999)
      local tickets = json.decode(vRP.getSData("lotto:tickets"))
      if not tickets then tickets = {} end
      local money = json.decode(vRP.getSData("lotto:money"))
      if not money then money = 0 end
      for user_id,u_tickets in pairs(tickets) do
         for i,ticket in pairs(u_tickets) do
            if no_winner and ticket == winner then
               vRP.setBankMoney(tonumber(user_id),money)
               vRP.setSData("lotto:money", json.encode(0))
               local identity = vRP.getUserIdentity(tonumber(user_id))
               TriggerClientEvent("chatMessage", -1, "[Loteria]", {255, 255, 0}, "E o ganhador da Loteria foi: ^2" .. identity.name .. " " .. identity.firstname .. "^0 com o bilhete N° ^2" ..winner.. " ^0e levou uma bolada no valor de ^2R$" ..money)
               no_winner = false
            end
         end
      end
    
      if no_winner then
         TriggerClientEvent("chatMessage", -1, "[Loteria]", {255, 255, 0}, "A loteria já está acumulada em: ^2R$" ..money)
      end
      vRP.setSData("lotto:time",json.encode(os.time()))
   end 
   TriggerClientEvent("chatMessage", -1, "[Loteria]", {255, 255, 0}, randomMsg[math.random(1,#randomMsg)])
   vRP.setSData("lotto:tickets",json.encode({}))
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CONTADOR DAS MENSAGENS DA LOTÉRICA NO CHAT
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
   while true do
      Wait(60000*5)
      tick_lotto()
   end
end)