function vRPbm.getArmour()
  	return GetPedArmour(GetPlayerPed(-1))
end

function vRPbm.setArmour(armour,vest)
  	local player = GetPlayerPed(-1)
  	if vest then
  		if(GetEntityModel(player) == GetHashKey("mp_m_freemode_01")) then
    		SetPedComponentVariation(player, 9, 4, 1, 2)
  		else 
    		if(GetEntityModel(player) == GetHashKey("mp_f_freemode_01")) then
      			SetPedComponentVariation(player, 9, 6, 1, 2)
    		end
  		end
  	end
  	local n = math.floor(armour)
  	SetPedArmour(player,n)
end

RegisterNetEvent("RemoveOxyTank")
AddEventHandler("RemoveOxyTank", function()
	if oxygenTank > 25.0 then
  		oxygenTank = 25.0
	end
end)

RegisterNetEvent("UseOxygenTank")
AddEventHandler("UseOxygenTank", function()
	oxygenTank = 100.0
end)

oxygenTank = 25.0
function vRPbm.equipScuba()
	SetEnableScuba(GetPlayerPed(-1),true)
	StatSetInt(GetHashKey("MP0_STAMINA"),900000,true)
	if oxygenTank > 0 then
  		SetPedDiesInWater(GetPlayerPed(-1), false)
  		if oxygenTank > 25.0 then
    		oxygenTank = oxygenTank - 0.001
  		else
    		oxygenTank = oxygenTank - 0.01
  		end
	else
  		SetPedDiesInWater(GetPlayerPed(-1), true)
	end
  
	if oxygenTank < 25.0 then
		oxygenTank = oxygenTank * 0.01
		if oxygenTank > 25.0 then
			oxygenTank = 25.0
		end
	end

	if oxygenTank > 25.0 and not oxyOn then
		oxyOn = true
	elseif oxyOn and oxygenTank <= 25.0 then
		oxyOn = false
	end
end

function vRPbm.unequipScuba()
	if not vRP.isInComa() then
		SetEnableScuba(GetPlayerPed(-1),false)
		StatSetInt(GetHashKey("MP0_LUNG_CAPACITY"),50,true)
		StatSetInt(GetHashKey("MP0_STAMINA"),50,true)
	end
end

local state_ready = false
AddEventHandler("playerSpawned",function()
	state_ready = false

	Citizen.CreateThread(function()
  		Citizen.Wait(30000)
  		state_ready = true
	end)
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(30000)
		if IsPlayerPlaying(PlayerId()) and state_ready then
			if vRPbm.getArmour() == 0 then
				if(GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_m_freemode_01")) or (GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_f_freemode_01")) then
					SetPedComponentVariation(GetPlayerPed(-1), 9, 0, 1, 2)
				end
			end
		end
	end
end)