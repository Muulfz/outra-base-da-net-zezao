-- fix barbershop green hair for now
ch_fixhair = {function(player,choice)
    local custom = {}
    local user_id = vRP.getUserId(player)
    local value = vRP.getUData(user_id,"vRP:head:overlay")
    if value ~= nil then
        custom = json.decode(value)
        BSclient.setOverlay(player,custom,true)
    end
end}

divers = {}
ch_scuba = {function(player,choice)
    if not vRPclient.isInComa(player) then
        local user_id = vRP.getUserId(player)
        if not divers[user_id] then
            divers[user_id] = vRPclient.getCustomization(player)
            local custom = json.decode('{"1":[0,0,2],"2":[1,0,2],"3":[3,0,2],"4":[3,0,2],"5":[1,0,2],"6":[3,0,2],"7":[0,0,2],"8":[1,0,2],"9":[7,0,2],"10":[0,0,0],"11":[0,0,2],"12":[0,0,0],"13":[0,2,0],"14":[0,2,255],"15":[0,2,229],"16":[0,2,255],"17":[0,2,255],"18":[0,2,255],"19":[0,2,255],"20":[0,2,0],"p4":[-1,0],"p5":[-1,0],"p6":[-1,0],"p7":[-1,0],"p9":[-1,0],"p8":[-1,0],"p10":[-1,0],"modelhash":225514697,"0":[0,2,0],"p3":[-1,0],"p2":[-1,0],"p1":[-1,0],"p0":[10,0]}') -- aqui vai a customizacao da scuba
            vRPclient.setCustomization(player,custom)
            BMclient.equipScuba(player)
        else
            vRPclient.setCustomization(player,divers[user_id])
            divers[user_id] = nil
            BMclient.unequipScuba(player)
        end
    end
end}

choice_player_check = {function(player,choice)
    local nplayer = vRPclient.getNearestPlayer(player,5)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
		TriggerClientEvent("pNotify:SendNotification", nplayer, {
			text = "Você está sendo revistado",
			type = "info",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})		
        local weapons = vRPclient.getWeapons(nplayer)
        local money = vRP.getMoney(nuser_id)
        local items = ""
        local data = vRP.getUserDataTable(nuser_id)
        if data and data.inventory then
            for k,v in pairs(data.inventory) do
                local item_name = vRP.getItemName(k)
                if item_name then
                    items = items.."<br />"..item_name.." ("..v.amount..")"
                end
            end
        end

        local weapons_info = ""
        for k,v in pairs(weapons) do
            weapons_info = weapons_info.."<br />"..k.." ("..v.ammo..")"
        end

        vRPclient.setDiv(player,"police_check",".div_police_check{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",lang.police.menu.check.info({money,items,weapons_info}))
        -- request to hide div
        local ok = vRP.request(player, "Devolver Identidade", 1000)
        vRPclient.removeDiv(player,"police_check")
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Nenhum jogador próximo",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
    end
end}

-- player store weapons
storeweap_cooldown = {}
function storeWeapCD()
    for k,v in pairs(storeweap_cooldown) do
        storeweap_cooldown[k] = nil
    end
    SetTimeout(1000,storeWeapCD)
end

storeWeapCD()
choice_store_weapons = {function(player, choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil and not storeweap_cooldown[user_id] then
        storeweap_cooldown[user_id] = true
        local weapons = vRPclient.getWeapons(player)
        for k,v in pairs(weapons) do
            vRP.giveInventoryItem(user_id, "wbody|"..k, 1, true)
            TriggerClientEvent("removeWeapon",player,k)
            if v.ammo > 0 then
                vRP.giveInventoryItem(user_id, "wammo|"..k, v.ammo, true)
            end
        end

        vRPclient.giveWeapons(player,{},true)
    end
end}