coordenadas = {
	[1] = {["X"] = 460.20, ["Y"] = -997.76, ["Z"] = 24.91},
	[2] = {["X"] = 460.03, ["Y"] = -994.12, ["Z"] = 24.91},
	[3] = {["X"] = 459.90, ["Y"] = -1001.12, ["Z"] = 24.91},
  }

  unjailed = {}
function jail_clock(target_id,timer)
  	local target = vRP.getUserSource(tonumber(target_id))
  	local users = vRP.getUsers()
  	local online = false
  	for k,v in pairs(users) do
		if tonumber(k) == tonumber(target_id) then
	  		online = true
		end
  	end
	  
	if online then
    	if timer>0 then
			TriggerClientEvent("pNotify:SendNotification", target, {
				text = "Tempo de sentença restante " .. timer .. " segundos",
				type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
      		vRP.setUData(tonumber(target_id),"vRP:jail:time",json.encode(timer))
	  		SetTimeout(60*1000, function()
				for k,v in pairs(unjailed) do
		  			if v == tonumber(target_id) then
	        			unjailed[v] = nil
		    			timer = 0
		  			end
				end
				vRP.setHunger(tonumber(target_id), 0)
				vRP.setThirst(tonumber(target_id), 0)
	    		jail_clock(tonumber(target_id),timer-1)
	  		end) 
    	else 
	  		BMclient.loadFreeze(target,false,true,true)
	  		SetTimeout(5000,function()
				BMclient.loadFreeze(target,false,false,false)
	  		end)
	  		vRPclient.teleport(target,425.7607421875,-978.73425292969,30.709615707397) -- coordenadas fora da prisão
			TriggerClientEvent("pNotify:SendNotification", target, {
				text = "Você foi libertado",
				type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
				animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
			})
	  		vRP.setUData(tonumber(target_id),"vRP:jail:time",json.encode(-1))
    	end
  	end
end

AddEventHandler("vRP:playerSpawn", function(user_id, source, first_spawn) 
  	local target = vRP.getUserSource(user_id)
  	SetTimeout(15000,function()
    	local custom = {}
    	local value = vRP.getUData(user_id,"vRP:jail:time")
	  	if value ~= nil then
	    	custom = json.decode(value)
	    	if custom ~= nil then
		  		if tonumber(custom) > 0 then
					BMclient.loadFreeze(target,false,true,true)
					SetTimeout(5000,function()
			  			BMclient.loadFreeze(target,false,false,false)
					end)
					local escolha = math.random(1,#coordenadas)
	  				vRPclient.teleport(target, coordenadas[escolha]["X"],coordenadas[escolha]["Y"],coordenadas[escolha]["Z"])
					TriggerClientEvent("pNotify:SendNotification", target, {
						text = "Termine de cumprir sua sentença",
						type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
						animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
					})
					vRP.setHunger(tonumber(user_id),0)
					vRP.setThirst(tonumber(user_id),0)
					vRP.setSono(tonumber(user_id),0)
					vRP.setNecessidades(tonumber(user_id),0)
					local target_id = "Logging In/Out"
					jail_clock(tonumber(user_id),tonumber(custom))
		  		end
	    	end
	  	end
  	end)
end)