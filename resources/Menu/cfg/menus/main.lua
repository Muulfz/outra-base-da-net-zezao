Citizen.CreateThread(function()
  	vRP.registerMenuBuilder("main", function(add, data)
    	Citizen.CreateThread(function()
      		local user_id = vRP.getUserId(data.player)
      		if user_id ~= nil then
        		local choices = {}
      
        		if vRP.hasPermission(user_id,"Menu.Jogador") then
          			choices["Menu Pessoal"] = ch_player_menu
        		end       
      
        		add(choices)
     	 	end
    	end)
  	end)
end)

Citizen.CreateThread(function()
  	vRP.registerMenuBuilder("pessoal", function(add, data)
    	Citizen.CreateThread(function()
      		local user_id = vRP.getUserId(data.player)
      		if user_id ~= nil then
        		local choices = {}
		
				if vRP.hasPermission(user_id,"Saquear.Cadaver") then
					choices["Saquear Cadaver"] = choice_loot
				end
	
				if vRP.hasPermission(user_id,"Bater.Carteira") then
					choices["Bater Carteira"] = ch_mug
				end
			
				if vRP.hasPermission(user_id,"Hackear.Jogador") then
					choices["Hackear"] = ch_hack
				end
			
				if vRP.hasPermission(user_id,"Arrombar.Carro") then
					choices["Arrombar Carro"] = ch_lockpickveh
				end	    
		
				add(choices)
      		end
    	end)
  	end)
end)