Citizen.CreateThread(function()
  	vRP.registerMenuBuilder("police", function(add, data)
    	Citizen.CreateThread(function()
      		local user_id = vRP.getUserId(data.player)
      		if user_id ~= nil then
        		local choices = {}
				
				if vRP.hasPermission(user_id,"Policial.Prender") then
          			choices["Prender"] = ch_jail
        		end
				
				if vRP.hasPermission(user_id,"Policial.Soltar") then
          			choices["Soltar"] = ch_unjail
       			end
      
        		if vRP.hasPermission(user_id,"Policial.Algemar") then
          			choices["Algemar"] = ch_handcuff
        		end

        		if vRP.hasPermission(user_id,"Policial.Spikes") then
          			choices["Tapete de Pregos"] = ch_spikes
				end
				
				--[[
        		if vRP.hasPermission(user_id,"Policial.Arrastar") then
          			choices["Arrastar"] = ch_drag
				end
				]]
      
        		if vRP.hasPermission(user_id,"Policial.Congelar") then
          			choices["Congelar"] = ch_freeze
					end
      
        		add(choices)
      		end
    	end)
  	end)
end)