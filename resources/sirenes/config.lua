vcf_files = {
	"POLICE.xml",
	"POLICE2.xml",
	"POLICE3.xml",
	"POLICE4.xml",
	"PRANGER.xml",
	"SHERIFF2.xml",
	"SHERIFF.xml",
	"trailft.xml",
	"trailrota.xml",
	"sw4rota.xml",
	"G5POLICIA.xml",
	"POLICEB.xml",
	"MCLARENPD.xml",
	"UPARMORHVDES.xml",
	"AMBULANCE.xml",
	"FBI2.xml",
	"ems_gs1200.xml",
	"fire1.xml",
	"fire3.xml",
	"firetruk.xml",
	"r1custom.xml",
	"DusterPM.xml",
	"gs350.xml",
	"sw4rota,xml",
	"L200.xml",
	"fiesta.xml",
	"corsapm.xml"
}

pattern_files = {
	"WIGWAG.xml",
	"WIGWAG3.xml",
	"FAST.xml",
	"COMPLEX.xml",
	"BACKFOURTH.xml",
	"BACKFOURTH2.xml",
	"T_ADVIS_RIGHT_LEFT.xml",
	"T_ADVIS_LEFT_RIGHT.xml",
	"T_ADVIS_BACKFOURTH.xml",
	"WIGWAG5.xml"
}

modelsWithTrafficAdvisor = {
	"FBI2"
}

modelsWithFireSiren =
{
    "FIRETRUK",
}


modelsWithAmbWarnSiren =
{   
    "AMBULANCE",
    "FIRETRUK",
    "LGUARD",
}

stagethreewithsiren = false
playButtonPressSounds = true
vehicleStageThreeAdvisor = {
    "FBI3",
}


vehicleSyncDistance = 150
envirementLightBrightness = 0.2

build = "master"

shared = {
	horn = 86,
}

keyboard = {
	modifyKey = 132,
	stageChange = 85,
	guiKey = 243,
	takedown = 245,
	siren = {
		tone_one = 157,
		tone_two = 158,
		tone_three = 160,
		dual_toggle = 164,
		dual_one = 165,
		dual_two = 159,
		dual_three = 161,
	},
	pattern = {
		primary = 246,
		secondary = 303,
		advisor = 182,
	},
}

controller = {
	modifyKey = 73,
	stageChange = 80,
	takedown = 74,
	siren = {
		tone_one = 173,
		tone_two = 85,
		tone_three = 172,
	},
}