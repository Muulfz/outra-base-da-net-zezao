local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
vRPtj = {}
Tunnel.bindInterface("caminhoneiro",vRPtj)

RegisterServerEvent('truckerJob:success') -- calls the event from client file
AddEventHandler('truckerJob:success', function(amount) -- handles the event
    local user_id = vRP.getUserId(source)
	local player = vRP.getUserSource(user_id)
    vRP.giveMoney(user_id,amount)
    vRPclient.notify(player,"Voce foi pago ~g~"..amount..".")
end)

function vRPtj.checkJOB()
  local user_id = vRP.getUserId(source)
  return vRP.hasPermission(user_id,"caminhoneiro.trabalhar")
end