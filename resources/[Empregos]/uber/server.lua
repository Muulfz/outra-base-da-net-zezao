-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIÁVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Zezao = {}
Tunnel.bindInterface("Uber",Zezao)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ATIVADO QUANDO PRESSIONA E
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent('Uber:Emprego')
AddEventHandler('Uber:Emprego', function()
	local source = source
	local user_id = vRP.getUserId(source)
    local player = vRP.getUserSource(user_id)
    if vRP.hasPermission(user_id,"Equipar.Uber") then
      	TriggerClientEvent('Uber:Equipar', source)
	else
      	TriggerClientEvent("pNotify:SendNotification", user_id, {
			text = "Você não é um Uber",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ATIVADO QUANDO PRESSIONA Y
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent('Uber:ForaEmprego')
AddEventHandler('Uber:ForaEmprego', function()
	local source = source
	local user_id = vRP.getUserId(source)
    local player = vRP.getUserSource(user_id)
    if vRP.hasPermission(user_id,"Equipar.Uber") then
		TriggerClientEvent('Uber:Sair', source)
    else
    	TriggerClientEvent("pNotify:SendNotification", user_id, {
			text = "Você não é um Uber",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENSAGEM QUE ENTROU EM SERVIÇO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent('Mensagem:DentroUber')
AddEventHandler('Mensagem:DentroUber', function()
	local source = source
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(tonumber(user_id))
	TriggerClientEvent('chatMessage', -1, 'Uber', {255, 0, 0},"O Uber ^1".. identity.firstname .. " " .. identity.name .. " ^7iniciou seu expediente.")
	TriggerClientEvent("pNotify:SendNotification", user_id, {
		text = "Você bateu o ponto, vá trabalhar",
		type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
		animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
	})
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENSAGEM QUE SAIU DE SERVIÇO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent('Mensagem:ForaUber')
AddEventHandler('Mensagem:ForaUber', function()
	local source = source
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(tonumber(user_id))
	TriggerClientEvent('chatMessage', -1, 'Ubers', {255, 0, 0},"O Uber ^1".. identity.firstname .. " " .. identity.name .. " ^7terminou seu expediente.")
	TriggerClientEvent("pNotify:SendNotification", user_id, {
		text = "Você finalizou seu expediente",
		type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
		animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
	})
end)