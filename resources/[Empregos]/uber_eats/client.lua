local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIAVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local pizzaria = { 
	x = -1285.73, y = -1387.15, z = 3.44
}
local pagamento = 0
local possibilidade = 0

local entregas = {
	[1] = {name = "Vinewood Hills",x = -1220.50, y = 666.95 , z = 143.10},
	[2] = {name = "Vinewood Hills",x = -1338.97, y = 606.31 , z = 133.37},
	[3] = {name = "Rockford Hills",x = -1051.85, y = 431.66 , z = 76.06 },
	[4] = {name = "Rockford Hills",x = -904.04 , y = 191.49 , z = 68.44 },
	[5] = {name = "Rockford Hills",x = -21.58  , y = -23.70 , z = 72.24 },
	[6] = {name = "Hawick"        ,x = -904.04 , y = 191.49 , z = 68.44 },
	[7] = {name = "Alta"          ,x = 253.10, y = -344.79, z = 44.92 },
	[8] = {name = "Pillbox Hills" ,x = 5.62    , y = -707.72, z = 44.97 },
	[9] = {name = "Mission Row"   ,x = 284.50  , y = -938.50 , z = 28.35},
	[10] ={name = "Rancho"        ,x = 411.59  , y = -1487.54, z = 29.14},
	[11] ={name = "Davis"         ,x = 85.19   , y = -1958.18, z = 20.12},
	[12] ={name ="Chamberlain Hills",x = -213.00, y =-1617.35 , z =37.35},
	[13] ={name ="La puerta"      ,x = -1015.65, y =-1515.05 ,z = 5.51}
}

local EmServico = false
local destino = 0
local IndoEntrega = false
local IndoPizzaria = false
local multiplicador_pagamento = 0.05
local pagamento = 0
local px = 0
local py = 0
local pz = 0
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BLIPS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local blips = {
	{title="Uber Eats", colour=66, id=376, x = -1285.73, y = -1387.15, z = 3.44},
}

Citizen.CreateThread(function()
	for _, info in pairs(blips) do
		info.blip = AddBlipForCoord(info.x, info.y, info.z)
		SetBlipSprite(info.blip, info.id)
		SetBlipDisplay(info.blip, 4)
		SetBlipScale(info.blip, 0.9)
		SetBlipColour(info.blip, info.colour)
		SetBlipAsShortRange(info.blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString(info.title)
		EndTextCommandSetBlipName(info.blip)
	end
end)

function IrCasa(entregas,destino)
	entrega = AddBlipForCoord(entregas[destino].x,entregas[destino].y, entregas[destino].z)
	SetBlipSprite(entrega, 1)
	SetNewWaypoint(entregas[destino].x,entregas[destino].y)
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THREADS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(0)
		local source = source
		local Coordenadas = GetEntityCoords(GetPlayerPed(-1))			
		local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, pizzaria.x, pizzaria.y, pizzaria.z, true)
		if Distancia < 10.0 then
			Opacidade = math.floor(255 - (Distancia * 20))
			Texto3D(pizzaria.x, pizzaria.y, pizzaria.z+1.5, "PRESSIONE ~y~[E] ~w~PARA TRABALHAR", Opacidade)
			DrawMarker(27,pizzaria.x,pizzaria.y,pizzaria.z, 0, 0, 0, 0, 0, 0, 1.5001, 1.5001, 0.6001,255,255,51, 200, 0, 0, 0, 0)
			if IsControlJustPressed(1,38) then
				TriggerServerEvent("zezao_uber_eats:permissao", source)
			end
		end
	end
end)

RegisterNetEvent("zezao:abc")
AddEventHandler("zezao:abc", function ()
	Citizen.CreateThread(function()
		while true do
			Citizen.Wait(0)
			if EmServico == false then
				local Coordenadas = GetEntityCoords(GetPlayerPed(-1))	
				local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, pizzaria.x, pizzaria.y, pizzaria.z, true)
				if Distancia < 5.0 then
					if IsControlJustPressed(1,38) then
						EmServico = true
						IndoEntrega = true
						destino = math.random(1, 13)
						px = entregas[destino].x
						py = entregas[destino].y
						pz = entregas[destino].z
						distancia = round(GetDistanceBetweenCoords(pizzaria.x, pizzaria.y, pizzaria.z, px,py,pz))
						pagamento = distancia * multiplicador_pagamento
						IrCasa(entregas,destino)
					end
				end
			end

			if IndoEntrega == true then
				destinol = entregas[destino].name
				local Coordenadas = GetEntityCoords(GetPlayerPed(-1))			
				local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, entregas[destino].x,entregas[destino].y,entregas[destino].z, true)
				drawTxt("Vá até ~y~"..destinol .." ~w~para entregar o pedido",4, 1, 0.45, 0.92, 0.70,255,255,255,255)
				if GetDistanceBetweenCoords(px,py,pz, GetEntityCoords(GetPlayerPed(-1),true)) < 10 then
					Opacidade = math.floor(255 - (Distancia * 20))
					Texto3D(entregas[destino].x,entregas[destino].y,entregas[destino].z+1.5, "PRESSIONE ~y~[E] ~w~PARA ENTREGAR", Opacidade)
					DrawMarker(27,entregas[destino].x,entregas[destino].y,entregas[destino].z+0.1, 0, 0, 0, 0, 0, 0, 1.5001, 1.5001, 0.6001,255,255,51, 200, 0, 0, 0, 0)
					if IsControlJustPressed(1,38) then
						possibilidade = math.random(1, 100)
						if (possibilidade > 50) then
							gorjeta = math.random(10, 100)
							exports.pNotify:SendNotification({
								text = "Toma aqui uma gorjeta meu jovem",
								type = "info",timeout = 3000,progressBar = false, layout = "zezao",queue = "left",
								animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
							})	
							Citizen.Wait(2000)						
							exports.pNotify:SendNotification({
								text = "Você recebeu " .. gorjeta .. " reais de gorjeta",
								type = "success",timeout = 3000,progressBar = false, layout = "zezao",queue = "left",
								animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
							})
							TriggerServerEvent("zezao_uber_eats:gorjeta", gorjeta)
						end
						IndoEntrega = false
						IndoPizzaria = true
						RemoveBlip(entrega)
						SetNewWaypoint(pizzaria.x,pizzaria.y)
					end
				end
			end

			if IndoPizzaria == true then
				drawTxt("Volte até a ~y~Central ~w~para ser pago",4, 1, 0.45, 0.92, 0.70,255,255,255,255)
				local Coordenadas = GetEntityCoords(GetPlayerPed(-1))			
				local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, -1292.2386474609,-1389.9288330078,4.5082898139954, true)
				if Distancia < 10 then
					Opacidade = math.floor(255 - (Distancia * 20))
					Texto3D(-1292.2386474609,-1389.9288330078,4.5082898139954+1.5, "PRESSIONE ~y~[E] ~w~PARA RECEBER", Opacidade)
					DrawMarker(27,-1292.2386474609,-1389.9288330078,3.6082898139954, 0, 0, 0, 0, 0, 0, 1.5001, 1.5001, 0.6001,255,255,51, 200, 0, 0, 0, 0)
					if IsControlJustPressed(1,38) and Distancia < 1.1 then
						exports.pNotify:SendNotification({
							text = "Você recebeu " .. math.ceil(pagamento) .. " reais pela entrega",
							type = "success",timeout = 3000,progressBar = false, layout = "zezao",queue = "left",
							animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
						})
						TriggerServerEvent("zezao_uber_eats:pagamento", pagamento)
						IndoEntrega = false
						IndoPizzaria = false
						EmServico = false
						pagamento = 0
						px = 0
						py = 0
						pz = 0
					end
				end
			end

			if IsEntityDead(GetPlayerPed(-1)) then
				EmServico = false
				destino = 0
				IndoEntrega = false
				IndoPizzaria = false
				pagamento = 0
				px = 0
				py = 0
				pz = 0
			end
		end
	end)
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUNÇÕES
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Texto3D(x,y,z, text, Opacidade)
	local onScreen,_x,_y=World3dToScreen2d(x,y,z)
	local px,py,pz=table.unpack(GetGameplayCamCoords())    
	if onScreen then
		SetTextScale(0.54, 0.54)
		SetTextFont(4)
		SetTextProportional(1)
		SetTextColour(255, 255, 255, Opacidade)
		SetTextDropshadow(0, 0, 0, 0, Opacidade)
		SetTextEdge(2, 0, 0, 0, 150)
		SetTextDropShadow()
		SetTextOutline()
		SetTextEntry("STRING")
		SetTextCentre(1)
		AddTextComponentString(text)
		DrawText(_x,_y)
	end
end

function drawTxt(text,font,centre,x,y,scale,r,g,b,a)
	SetTextFont(font)
	SetTextProportional(0)
	SetTextScale(scale, scale)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextCentre(centre)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x , y)
end

function round(num, numDecimalPlaces)
	local mult = 5^(numDecimalPlaces or 0)
	return math.floor(num * mult + 0.5) / mult
end