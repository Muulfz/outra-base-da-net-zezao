-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIÁVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")

RegisterServerEvent("zezao_uber_eats:permissao")
AddEventHandler("zezao_uber_eats:permissao", function ()
	local source = source
    local user_id = vRP.getUserId(source)
    if vRP.hasPermission(user_id,"entregar.comida") then
    	TriggerClientEvent("zezao:abc", source)
    else
    	TriggerClientEvent("pNotify:SendNotification", source, {
            text = "Você não é um entregador de comida",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    end
end)

RegisterServerEvent("zezao_uber_eats:pagamento")
AddEventHandler("zezao_uber_eats:pagamento",function(pagamento)
    local sourcePlayer = tonumber(source)
    local user_id = vRP.getUserId(source)
    vRP.tryPayment(user_id, pagamento)    
end)

RegisterServerEvent("zezao_uber_eats:gorjeta")
AddEventHandler("zezao_uber_eats:gorjeta",function(gorjeta)
    local sourcePlayer = tonumber(source)
    local user_id = vRP.getUserId(source)
    vRP.tryPayment(user_id, gorjeta)    
end)