-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIÁVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Zezao = {}
Tunnel.bindInterface('policial',Zezao)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ATIVADO QUANDO PRESSIONA E
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent('Policial:Emprego')
AddEventHandler('Policial:Emprego', function()
	local source = source
	local user_id = vRP.getUserId(source)
    local player = vRP.getUserSource(user_id)
    if vRP.hasPermission(user_id,"Equipar.Recruta") then
      	TriggerClientEvent('Policial:Recruta', source)
      	vRPclient._giveWeapons(player,{
            ["WEAPON_COMBATPISTOL"] = {ammo=250},
			["WEAPON_STUNGUN"] = {ammo=1},
	  		["WEAPON_NIGHTSTICK"] = {ammo=1}, 
	  		["WEAPON_FLASHLIGHT"] = {ammo=1},
			["WEAPON_SMG"] = {ammo=200},
			["WEAPON_PUMPSHOTGUN"] = {ammo=50}
        })
	elseif vRP.hasPermission(user_id,"Equipar.Capitao") then
      	TriggerClientEvent('Policial:Capitao', source)
      	vRPclient._giveWeapons(player,{
			["WEAPON_COMBATPISTOL"] = {ammo=200},
			["WEAPON_CARBINERIFLE"] = {ammo=250},
			["WEAPON_STUNGUN"] = {ammo=1},
	  		["WEAPON_PUMPSHOTGUN"] = {ammo=50},
	  		["WEAPON_NIGHTSTICK"] = {ammo=1},
	  		["WEAPON_FLASHLIGHT"] = {ammo=1}
		})
	elseif vRP.hasPermission(user_id,"Equipar.Sargento") then
      	TriggerClientEvent('Policial:Sargento', source)
      	vRPclient._giveWeapons(player,{
			["WEAPON_COMBATPISTOL"] = {ammo=200},
			["WEAPON_CARBINERIFLE"] = {ammo=250},
			["WEAPON_STUNGUN"] = {ammo=1},
	  		["WEAPON_PUMPSHOTGUN"] = {ammo=50},
	  		["WEAPON_NIGHTSTICK"] = {ammo=1},
	  		["WEAPON_FLASHLIGHT"] = {ammo=1}
		})
	elseif vRP.hasPermission(user_id,"Equipar.Soldado") then
      	TriggerClientEvent('Policial:Soldado', source)
      	vRPclient._giveWeapons(player,{
			["WEAPON_COMBATPISTOL"] = {ammo=250},
			["WEAPON_STUNGUN"] = {ammo=1},
	  		["WEAPON_NIGHTSTICK"] = {ammo=1}, 
	  		["WEAPON_FLASHLIGHT"] = {ammo=1},
			["WEAPON_SMG"] = {ammo=200},
			["WEAPON_PUMPSHOTGUN"] = {ammo=100}
		})
	elseif vRP.hasPermission(user_id,"Equipar.Cabo") then
      	TriggerClientEvent('Policial:Cabo', source)
      	vRPclient._giveWeapons(player,{
			["WEAPON_COMBATPISTOL"] = {ammo=250},
			["WEAPON_STUNGUN"] = {ammo=1},
	  		["WEAPON_NIGHTSTICK"] = {ammo=1}, 
	  		["WEAPON_FLASHLIGHT"] = {ammo=1},
			["WEAPON_SMG"] = {ammo=200},
			["WEAPON_PUMPSHOTGUN"] = {ammo=100}
		})
	elseif vRP.hasPermission(user_id,"Equipar.Major") then
      	TriggerClientEvent('Policial:Major', source)
      	vRPclient._giveWeapons(player,{
			["WEAPON_COMBATPISTOL"] = {ammo=250},
			["WEAPON_SPECIALCARBINE"] = {ammo=250},
			["WEAPON_STUNGUN"] = {ammo=1},
			["WEAPON_BULLPUPSHOTGUN"] = {ammo=100},
	  		["WEAPON_NIGHTSTICK"] = {ammo=1},
	  		["WEAPON_FLASHLIGHT"] = {ammo=1}
		})
	elseif vRP.hasPermission(user_id,"Equipar.Coronel") then
      	TriggerClientEvent('Policial:Coronel', source)
      	vRPclient._giveWeapons(player,{
			["WEAPON_COMBATPISTOL"] = {ammo=250},
			["WEAPON_SPECIALCARBINE"] = {ammo=250},
			["WEAPON_STUNGUN"] = {ammo=1},
			["WEAPON_BULLPUPSHOTGUN"] = {ammo=100},
	  		["WEAPON_NIGHTSTICK"] = {ammo=1},
	  		["WEAPON_FLASHLIGHT"] = {ammo=1}
		})
	elseif vRP.hasPermission(user_id,"Equipar.Tencoronel") then
      	TriggerClientEvent('Policial:Tencoronel', source)
      	vRPclient._giveWeapons(player,{
			["WEAPON_COMBATPISTOL"] = {ammo=250},
			["WEAPON_SPECIALCARBINE"] = {ammo=250},
			["WEAPON_STUNGUN"] = {ammo=1},
			["WEAPON_BULLPUPSHOTGUN"] = {ammo=100},
	  		["WEAPON_NIGHTSTICK"] = {ammo=1},
	  		["WEAPON_FLASHLIGHT"] = {ammo=1}
		})
	else
      	TriggerClientEvent("pNotify:SendNotification", user_id, {
			text = "Você não é um policial",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ATIVADO QUANDO PRESSIONA Y
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent('Policial:ForaEmprego')
AddEventHandler('Policial:ForaEmprego', function()
	local source = source
	local user_id = vRP.getUserId(source)
    local player = vRP.getUserSource(user_id)
    if vRP.hasPermission(user_id,"Equipar.Recruta") then
		TriggerClientEvent('Policial:SairRecruta', source)
	elseif vRP.hasPermission(user_id,"Equipar.Capitao") then
		  TriggerClientEvent('Policial:SairCapitao', source)
    elseif vRP.hasPermission(user_id,"Equipar.Cabo") then
		  TriggerClientEvent('Policial:SairCabo', source)
    elseif vRP.hasPermission(user_id,"Equipar.Major") then
		  TriggerClientEvent('Policial:SairMajor', source)
    elseif vRP.hasPermission(user_id,"Equipar.Tencoronel") then
		  TriggerClientEvent('Policial:SairTencoronel', source)
	elseif vRP.hasPermission(user_id,"Equipar.Coronel") then
		  TriggerClientEvent('Policial:SairCoronel', source)	  
    elseif vRP.hasPermission(user_id,"Equipar.Soldado") then	
		  TriggerClientEvent('Policial:SairSoldado', source)
	elseif vRP.hasPermission(user_id,"Equipar.Sargento") then
      	TriggerClientEvent('Policial:SairSargento', source)
    else
    	TriggerClientEvent("pNotify:SendNotification", user_id, {
			text = "Você não é um policial",
			type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
    end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENSAGEM QUE ENTROU EM SERVIÇO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent('Mensagem:Dentro')
AddEventHandler('Mensagem:Dentro', function()
	local source = source
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(tonumber(user_id))
	TriggerClientEvent('chatMessage', -1, 'COPOM', {0, 0, 255},"O policial ^1".. identity.firstname .. " " .. identity.name .. " ^7iniciou seu expediente.")
	TriggerClientEvent("pNotify:SendNotification", user_id, {
		text = "Você bateu o ponto, vá trabalhar",
		type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
		animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
	})
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MENSAGEM QUE SAIU DE SERVIÇO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RegisterServerEvent('Mensagem:Fora')
AddEventHandler('Mensagem:Fora', function()
	local source = source
	local user_id = vRP.getUserId(source)
	local identity = vRP.getUserIdentity(tonumber(user_id))
	TriggerClientEvent('chatMessage', -1, 'COPOM', {0, 0, 255},"O policial ^1".. identity.firstname .. " " .. identity.name .. " ^7terminou seu expediente.")
	TriggerClientEvent("pNotify:SendNotification", user_id, {
		text = "Você finalizou seu expediente",
		type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
		animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
	})
end)