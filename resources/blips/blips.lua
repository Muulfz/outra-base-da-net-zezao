local blips = {
    {nome = "Ammu Nation",                cor = 1,  id = 156, x = 18.494960784912,    y = -1109.8464355469,  z = 29.797029495239 },
    {nome = "Ammu Nation",                cor = 1,  id = 156, x = 812.21673583984,    y = -2153.23828125,    z = 29.618995666504 },
    {nome = "Ammu Nation",                cor = 1,  id = 156, x = 843.78131103516,    y = -1025.6691894531,  z = 28.194860458374 },
    {nome = "Campo de Maconha",           cor = 2, id = 140, x = 2222.228515625,     y = 5577.2719726563,   z = 52.840530395508 },
    {nome = "Campo de Cocaina",           cor = 4,  id = 501, x = 1998.6602783203,    y = 4839.1049804688,   z = 43.548267364502 },
	{nome = "Campo de LSD",               cor = 4,  id = 499, x = 80.17179107666,   y = 3711.6618652344,   z = 40.670394897461 },
    {nome = "Laboratório de Cocaína",     cor = 4,  id = 501, x = 824.03259277344,y = -2997.0610351563,z = 6.0209364891052 },
    {nome = "Laboratório de Maconha",     cor = 2, id = 140, x = 1219.7586669922,y = -3201.6318359375,z = 5.5280680656433 },
	{nome = "Laboratório de LSD",         cor = 4, id = 499, x = 764.65142822266,y = -3202.8679199219,z = 6.0672149658203 },
    {nome = "Coleta de Armas",            cor = 40, id = 478, x = 138.78096008301,    y = -3110.8371582031,  z = 5.8963084220886 },
    {nome = "Aprimoramento de Armas",     cor = 40, id = 150, x = -52.597068786621,   y = -2523.1508789063,  z = 7.4011702537537 },
    {nome = "Biqueira (Maconha)",         cor = 2,  id = 140, x = 1343.2203369141,y = -542.49755859375,z = 77.205986022949 },
    {nome = "Biqueira (Cocaína)",         cor = 4,  id = 501, x = 771.74835205078,   y = -260.4892578125,   z = 68.945831298828 },
	{nome = "Biqueira (LSD)",             cor = 4,  id = 499, x = 471.88064575195,   y = -1690.6274414063,   z = 29.381868362427 },
    {nome = "Lavanderia (Dinheiro Sujo)", cor = 40, id = 500, x = -559.01287841797,   y = -1803.5979003906,  z = 22.608966827393 },
    {nome = "Escavação (Cobre)",          cor = 21, id = 89,  x = -494.81127929688,   y = -1752.521484375,   z = 18.316606521606 },
    {nome = "Fundição (Aço)",             cor = 62, id = 76,  x = 1114.9693603516,    y = -2004.0101318359,  z = 35.439395904541 },
    {nome = "Fabricação de Munição",      cor = 17, id = 436, x = 2330.2780761719,    y = 2572.0107421875,   z = 46.679504394531 },
    {nome = "Academia",                   cor = 59, id = 311, x = -1202.9625244146,   y = -1566.14086914063, z = 4.6104063987731 },
	{nome = "Coletar Tartaruga",          cor = 28, id = 365, x = -2118.327148437,    y = -1218.7764892578,  z = -141.2243804916 },
	{nome = "Capão Redondo",              cor = 64, id = 437, x = 829.61578369141, y = -328.53479003906, z = 56.596740722656 },
	{nome = "Heliópolis",                 cor = 1, id = 378, x = 1264.3389892578, y = -545.43566894531, z = 69.126182556152 },
	{nome = "Paraísopolis",               cor = 38, id = 310, x = 1395.4786376953, y = -1578.9244384766, z = 56.882274627686 },
	{nome = "Brasilandia",                cor = 4, id = 84, x = 425.57098388672, y = -1783.1862792969, z = 28.782348632813 },
    {nome = "Garagem Caminhão",           cor = 43, id = 477, x = 187.60931396484, y = 6396.5048828125, z = 31.383361816406 },
    {nome = "Vender Tartaruga",           cor = 1, id = 197,  x = -1816.2939453125,	  y = -1192.9519042969,	 z = 14.305061340332 },
    {nome = "Coletar Pedaço de Corpo",    cor = 4, id = 303,  x = -1763.4938964844,   y = -262.74224853516,  z = 48.201850891113 },
    {nome = "Processar Pedaço De Corpo",  cor = 4, id = 303,  x = -85.136779785156,   y = 6230.9106445313,  z = 31.090675354004 },
    {nome = "Vender Corpo",               cor = 4, id = 303,  x = 981.30853271484,   y = -2122.8718261719,  z = 30.475238800049 },
	{nome = "Praça",                      cor = 46, id = 480, x = 178.50614929199,y = -1012.1129150391,z = 29.185678482056 },
    {nome = "Tequilala",                  cor = 46, id = 93, x = -565.67626953125,y = 272.82727050781,z = 83.019729614258 },
    {nome = "Campo Madereiro",            cor = 1, id = 21, x = -808.41857910156,y = 5389.7744140625,z = 32.517017364502 },
    {nome = "Processamento de Madeira",   cor = 1, id = 21, x = -533.40118408203,y = 5291.4008789063,z = 72.214508056641 },
    {nome = "Venda de Madeira",           cor = 1, id = 21, x = 3513.6433105469,y = 3755.4765625,z = 27.957712173462 },
    {nome = "Campo de Esmeraldas",        cor = 1, id = 11, x = 1118.4421386719,y = 2189.0473632813,z = 46.02225112915 },
    {nome = "Processamento de Esmeralda", cor = 1, id = 11, x = -1602.8891601563,y = 5205.255859375,z = 4.310088634491 },
    {nome = "Mafia",                      cor = 4, id = 567, x = -369.68997192383,y = -117.96774291992,z = 38.808868408203 },
    {nome = "Yakuza",                     cor = 4, id = 569, x = 2358.6789550781,y = 3126.732421875,z = 48.208744049072 },
    {nome = "Venda de Esmeralda",         cor = 1, id = 11, x = -631.35131835938,y = -229.02763366699,z = 38.057014465332 }
}

Citizen.CreateThread(function()
    for _, info in pairs(blips) do
        info.blip = AddBlipForCoord(info.x, info.y, info.z)
        SetBlipSprite(info.blip, info.id)
        SetBlipDisplay(info.blip, 4)
        SetBlipScale(info.blip, 0.9)
        SetBlipColour(info.blip, info.cor)
        SetBlipAsShortRange(info.blip, true)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString(info.nome)
        EndTextCommandSetBlipName(info.blip)
    end
end)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THREAD PARA COLOCAR DRAWMARKER ONDE QUISER A PARTIR DE UMA ARRAY (MARCAS PEQUENAS)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local marcacoes = {
    -- O ultimo é sempre sem virgula, só para lembrar...
    {x = 1273.5339355469, y = -1711.4639892578, z = 54.771507263184} -- Hacker (Dinheiro Sujo)
}

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        for k in pairs(marcacoes) do
            DrawMarker(27, marcacoes[k].x, marcacoes[k].y, marcacoes[k].z, 0, 0, 0, 0, 0, 0, 1.001, 1.0001, 0.5001, 255, 255, 255, 155, 0, 0, 0, 0)
        end
    end
end)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THREAD PARA COLOCAR DRAWMARKER ONDE QUISER A PARTIR DE UMA ARRAY (MARCAS GRANDES)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local marcacoes = {
    -- O ultimo é sempre sem virgula, só para lembrar...
    --{x = 215.124,            y = -791.377,          z = 29.90 }

    }

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        for k in pairs(marcacoes) do
            DrawMarker(27, marcacoes[k].x, marcacoes[k].y, marcacoes[k].z, 0, 0, 0, 0, 0, 0, 3.001, 3.0001, 0.5001, 255, 255, 255, 155, 0, 0, 0, 0)
        end
    end
end)
