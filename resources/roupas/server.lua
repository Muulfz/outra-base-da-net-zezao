local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","roupas")
vRPgc = Tunnel.getInterface("roupas","roupas")

RegisterServerEvent('clothes:checkmoney')
AddEventHandler('clothes:checkmoney', function()
    local user_id = vRP.getUserId(source)
    local player = vRP.getUserSource(user_id)
    if vRP.tryPayment(user_id,100) then
		TriggerClientEvent('clothes:success', player)
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Você pagou 100 reais",
			type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
	else
		TriggerClientEvent("pNotify:SendNotification", player, {
			text = "Você não tem dinheiro suficiente",
			type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
			animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
		})
    end
end)