--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

Tunnel = module("vrp", "lib/Tunnel")
Proxy = module("vrp", "lib/Proxy")
cfg = module("roubos", "cfg/robbery")
vRProb = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Tunnel.bindInterface("roubos",vRProb)
Proxy.addInterface("roubos",vRProb)
ROBclient = Tunnel.getInterface("roubos")

robbers = {}
lastrobbed = {}

function vRProb.cancelRobbery(robb)
    if(robbers[source])then
        robbers[source] = nil
        TriggerClientEvent('chatMessage', -1, "NEWS", {255, 0, 0}, "Roubo cancelado em " .. cfg.robbery[robb].name .. ".")
    end
end

function vRProb.startRobbery(robb)
    local canceled = false
    local player = source
    local user_id = vRP.getUserId(player)
    local cops = vRP.getUsersByPermission(cfg.cops)
    local robbery = cfg.robbery[robb]
    if vRP.hasPermission(user_id,cfg.cops) then
        ROBclient.robberyComplete(player)
        TriggerClientEvent("pNotify:SendNotification", player, {
            text = "Que feio, policial tentando roubar",
            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
        })
    else
        if robbery then
            if #cops >= robbery.cops then
                if lastrobbed[robb] then
                    local past = os.time() - lastrobbed[robb]
                    local wait = robbery.rob + robbery.wait
                    if past <  wait then
                        ROBclient.robberyComplete(player)
                        TriggerClientEvent("pNotify:SendNotification", player, {
                            text = "Este lugar ja foi assaltado recentemente. Aguarde " .. wait - past .. " segundos para assaltar novamente.",
                            type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                            animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                        })
                        canceled = true
                    end
                end
                if not canceled then
                    TriggerClientEvent('chatMessage', -1, "NEWS", {255, 0, 0}, "Assalto iniciado em " ..robbery.name .. ".")
                    TriggerClientEvent("pNotify:SendNotification", player, {
                        text = "Você iniciou um roubo em " ..robbery.name .. ".",
                        type = "info",progressBar = false,timeout = 7000,layout = "zezao",queue = "left",
                        animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                    })
                    lastrobbed[robb] = os.time()
                    robbers[player] = robb
                    local savedSource = player
                    SetTimeout(robbery.rob*1000, function()
                        if(robbers[savedSource])then
                            if(user_id)then
                                local reward = math.random(robbery.min,robbery.max)
                                vRP.giveInventoryItem(user_id,"Dinheiro Sujo",reward,true)
                                TriggerClientEvent('chatMessage', -1, "NEWS", {255, 0, 0}, "Chega ao fim o roubo ao" .. robbery.name .. ".")
                                TriggerClientEvent("pNotify:SendNotification", savedSource, {
                                    text = "Roubo concluido, você recebeu " .. reward .. " reais.",
                                    type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                                })
                                ROBclient.robberyComplete(savedSource)
                            end
                        end
                    end)
                end
            else
                ROBclient.robberyComplete(player)
                TriggerClientEvent("pNotify:SendNotification", player, {
                    text = "Não há policiais suficientes online",
                    type = "alert",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
                    animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
                })
            end
        end
    end
end
