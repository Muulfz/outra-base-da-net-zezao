-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- VARIÁVEIS LOCAIS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")


RegisterServerEvent("K9:SendLanguage")
AddEventHandler("K9:SendLanguage", function()
    local s = source
    TriggerClientEvent("K9:UpdateLanguage", s, K9Config.Languages[K9Config.LanguageChoice])
end)

RegisterServerEvent("K9:RequestOpenMenu")
AddEventHandler("K9:RequestOpenMenu", function()
    local src = source
    if K9Config.AbrirMenu then
        for b = 1, #K9Config.SkinsBloqueadas do
            TriggerClientEvent("K9:OpenMenu", src, K9Config.AbrirMenu, K9Config.SkinsBloqueadas)
        end
    else

    end
end)

RegisterServerEvent("K9:RequestVehicleToggle")
AddEventHandler("K9:RequestVehicleToggle", function()
    local src = source
    print("Requested Vehicle Toggle")
    TriggerClientEvent("K9:ToggleVehicle", src, K9Config.VehicleRestriction, K9Config.VehiclesList)
end)

--[[ Functions ]]--
function GetPlayerId(type, id)
    local identifiers = GetPlayerIdentifiers(id)
    for i = 1, #identifiers do
        if string.find(identifiers[i], type, 1) ~= nil then
            return identifiers[i]
        end
    end
    return false
end