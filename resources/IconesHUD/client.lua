local Proxy = module("vrp", "lib/Proxy")
vRP = Proxy.getInterface("vRP")

Citizen.CreateThread(function()
  while true do
     Citizen.Wait(1)
     local Jogador = GetPlayerPed(-1)
     if (IsPedInAnyVehicle(Jogador, false)) and IconesPassageiro(GetPlayerPed(-1)) == -1 then
         css = [[
            .div_icone_gasolina{
            position: absolute;
            content: url(https://i.imgur.com/EsOSX6h.png);
            height: 30px;
            width: 30px;
            top: 825px;
            left: 215px;
         }
         .div_icone_motor{
            position: absolute;
            content: url(https://i.imgur.com/H2PI7Ar.png);
            height: 30px;
            width: 30px;
            top: 825px;
            left: 13px;
         }   
         ]]

         vRP.setDiv("icone_motor",css)
         vRP.setDiv("icone_gasolina",css)

      else

         vRP.removeDiv("icone_motor",css)
         vRP.removeDiv("icone_gasolina",css)

     end
  end
end)

function IconesPassageiro(Jogador)
    local MeuCarro = GetVehiclePedIsIn(Jogador, false)
    for i = -2, GetVehicleMaxNumberOfPassengers(MeuCarro) do
        if(GetPedInVehicleSeat(MeuCarro, i) == Jogador) then return i end
    end
    return - 2
end