local m = {}
m.delay = 10

m.prefix = '^0[GUARULHOS] '
m.suffix = ''
m.messages = {
    '^1Comandos Basicos: ^2/rg',
    '^1Entre no discord do servidor: https://discord.gg/Hqka9sG',
    '^1Comandos de chat de texto: /twitter, /190, /192, /olx, /ilegal',
    '^1Regras do servidor em https://discord.gg/Hqka9sG',
    '^1Favor não spamar nos chats!',
    '^1Respeite as regras do servidor e respeite os outros jogadores! DISCORD: https://discord.gg/Hqka9sG',
    '^1Todos os bugs, hacks, algo do tipo, informar um ADM',
    '^1Denuncias apenas com videos/fotos na sala de denuncias!',
}

m.ignorelist = {

}

local playerIdentifiers
local enableMessages = true
local timeout = m.delay * 1000 * 60
local playerOnIgnoreList = false
RegisterNetEvent('va:setPlayerIdentifiers')
AddEventHandler('va:setPlayerIdentifiers', function(identifiers)
    playerIdentifiers = identifiers
end)
Citizen.CreateThread(function()
    while playerIdentifiers == {} or playerIdentifiers == nil do
        Citizen.Wait(1000)
        TriggerServerEvent('va:getPlayerIdentifiers')
    end
    for iid in pairs(m.ignorelist) do
        for pid in pairs(playerIdentifiers) do
            if m.ignorelist[iid] == playerIdentifiers[pid] then
                playerOnIgnoreList = true
                break
            end
        end
    end
    if not playerOnIgnoreList then
        while true do
            for i in pairs(m.messages) do
                if enableMessages then
                    chat(i)

                end
                Citizen.Wait(timeout)
            end

            Citizen.Wait(0)
        end
    else

    end
end)
function chat(i)
    TriggerEvent('chatMessage', '', {255,255,255}, m.prefix .. m.messages[i] .. m.suffix)
end
RegisterCommand('automessage', function()
    enableMessages = not enableMessages
    if enableMessages then
        status = '^2enabled^5.'
    else
        status = '^1disabled^5.'
    end
    TriggerEvent('chatMessage', '', {255, 255, 255}, '^5[GUARULHOS] as mensagens automáticas são agora ' .. status)
end, false)
--------------------------------------------------------------------------
