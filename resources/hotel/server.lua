local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")

coordenadas = {
  [1] = {["X"] = 154.080, ["Y"] = -1004.865, ["Z"] = -98.419},
  [2] = {["X"] = 154.080, ["Y"] = -1004.865, ["Z"] = -98.419},
}

RegisterServerEvent("hotel:pagar")
AddEventHandler("hotel:pagar", function()
  local source = source
  local user_id = vRP.getUserId(source)
  local player = vRP.getUserSource(user_id)
  if vRP.tryPayment(user_id, 250) then
    local escolha = math.random(1,#coordenadas)
	  vRPclient.teleport(source, coordenadas[escolha]["X"],coordenadas[escolha]["Y"],coordenadas[escolha]["Z"])
    vRPclient._playAnim(source,false,{task="WORLD_HUMAN_BUM_SLUMPED"},false)
    TriggerClientEvent("pNotify:SendNotification", player, {
      text = "Você pagou 250 reais pela diária",
      type = "success",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
      animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
    })
    SetTimeout(15000, function()
      local source = vRP. getUserSource(user_id)
      vRPclient._stopAnim(source,false)
      vRPclient.teleport(source, 169.138,-567.485,44.272)
      vRP.varySono(user_id,-100)
      vRP.varyNecessidades(user_id,-100)
    end)
  else
    TriggerClientEvent("pNotify:SendNotification", player, {
      text = "Você não tem dinheiro suficiente",
      type = "error",progressBar = false,timeout = 3000,layout = "zezao",queue = "left",
      animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"}
    })
  end
end)