local cfg = {}

cfg.salario = {
  ["salario.recruta"] = 2750,
  ["salario.soldado"] = 3750,
  ["salario.cabo"] = 4750,
  ["salario.sargento"] = 5750,
  ["salario.capitao"] = 6750,
  ["salario.major"] = 7750,
  ["salario.tencoronel"] = 8750,
  ["salario.coronel"] = 10500,
  ["salario.grpae"] = 9750,
  ["salario.rocam"] = 3900,
  ["salario.bombeiro"] = 12000,
  ["salario.uber"] = 3000,
  ["salario.ubereats"] = 2500,
  ["salario.sedex"] = 2250,
  ["salario.vip"] = 8000,
  ["salario.mecanico"] = 2000
}

cfg.imposto = {
  ["contas.cidadao"] = 100
}

return cfg