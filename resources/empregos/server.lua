-- ###################################
--
--       Credits: Shadow
--
-- ###################################
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRPjb = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
Tunnel.bindInterface("empregos",vRPjb)

function vRPjb.jobCaminhoneiro()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "usuario") then
	vRP.addUserGroup(user_id, "caminhoneiro")
	vRPclient._notify(source, "Agora você é um Caminhoneiro")
  end
end

function vRPjb.jobMecanico()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "usuario") then
	vRP.addUserGroup(user_id, "mecanico")
	vRPclient._notify(source, "Agora você é um Mecânico")
  end
end

function vRPjb.jobTaxi()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "usuario") then
	vRP.addUserGroup(user_id, "taxi")
	vRPclient._notify(source, "Agora você é um Uber")
  end
end

function vRPjb.jobPescador()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "usuario") then
	vRP.addUserGroup(user_id, "pescador")
	vRPclient._notify(source, "Agora você é um pescador")
  end
end

function vRPjb.jobEntregadorComida()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "usuario") then
	vRP.addUserGroup(user_id, "entregador de comida")
	vRPclient._notify(source, "Agora você é um entregador de comida")
  end
end

function vRPjb.jobDesempregado()
  local source = source
  local user_id = vRP.getUserId(source)
  
  if vRP.hasGroup(user_id, "usuario") then
	vRP.addUserGroup(user_id, "desempregado")
	vRPclient._notify(source, "Agora você está desempregado, vá curtir a vida")
  end
end