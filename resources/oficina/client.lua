local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")
PSserver = Tunnel.getInterface("oficina")

local reparando = false
local segundos = 0

local Oficinas = {
    [1]  = { ["X"] = -31.46, ["Y"] = -1089.53, ["Z"] = 25.62},
    [2]  = { ["X"] = 2008.79, ["Y"] = 3799.93, ["Z"] = 33.18},
    [3]  = { ["X"] = 110.58, ["Y"] = 6626.63, ["Z"] = 32.78},
}

RegisterNetEvent("zezao:pelamor")
AddEventHandler("zezao:pelamor", function ()
	local ped = GetPlayerPed(-1)
	local MeuCarro = GetVehiclePedIsUsing(ped)
	SetVehicleEngineHealth(MeuCarro, 1000)
	SetVehicleFixed(MeuCarro)
	SetVehicleDirtLevel(veh, 0.0)
  	SetVehicleUndriveable(veh, false)
end)

RegisterNetEvent("zezao:desligarveiculo")
AddEventHandler("zezao:desligarveiculo", function ()
	reparando = true
	local ped = GetPlayerPed(-1)
	local MeuCarro = GetVehiclePedIsUsing(ped)
	SetVehicleUndriveable(MeuCarro, true)
end)

Citizen.CreateThread(function()
	while true do
		if reparando then
			Citizen.Wait(1000)
			if(segundos > 0)then
				segundos = segundos - 1
			end
		end
		Citizen.Wait(0)
	end
end)

function TextoReparar(x,y ,width,height,scale, text, r,g,b,a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
	    SetTextOutline()
	end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		for i = 1, #Oficinas do
			local Coordenadas = GetEntityCoords( GetPlayerPed(-1) )
			local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, Oficinas[i]["X"], Oficinas[i]["Y"], Oficinas[i]["Z"], true)
			local ped = GetPlayerPed(-1)
			local MeuCarro = GetVehiclePedIsUsing(ped)
			local vida = GetEntityHealth(MeuCarro)
			if Distancia < 10.0 then
				Opacidade = math.floor(255 - (Distancia * 30))
				Texto3D(Oficinas[i]["X"], Oficinas[i]["Y"], Oficinas[i]["Z"]+1, "Oficina Mecânica", Opacidade)
				Texto3D(Oficinas[i]["X"], Oficinas[i]["Y"], Oficinas[i]["Z"]+0.5, "Pressione ~r~[ E ]", Opacidade)
				DrawMarker(27, Oficinas[i]["X"], Oficinas[i]["Y"], Oficinas[i]["Z"], 0, 0, 0, 0, 0, 0, 3.0001,3.0001,1.5001, 255, 255, 255, Opacidade, 0, 0, 2, 0, 0, 0, 0)
				if IsControlJustPressed(1, 38) then
					if (IsPedSittingInAnyVehicle(ped)) then 
						if vida == 1000 then
							TriggerEvent('chatMessage', "Mecânico", {255, 0, 0}, "Este carro não precisa de conserto.")
						else
							TriggerServerEvent("zezao:naosei", source)
						end							
					else
						TriggerEvent('chatMessage', "Mecânico", {255, 0, 0}, "Você não está em um carro.")
					end
				end
			end
		end
	end
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TEXTO 3D
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Texto3D(x,y,z, text, Opacidade)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())    
    if onScreen then
        SetTextScale(1.0, 1.0)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, Opacidade)
        SetTextDropshadow(0, 0, 0, 0, Opacidade)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end