local blips = { 
{name="Hospital", x= 338.85, y= -1394.56, z= 31.51},
}

Lugares = {
    [1]  = { ["X"] = 242.26, ["Y"] = -1377.38, ["Z"] = 40.53}, -- Médico
}

function Texto3D(x,y,z, text, Opacidade)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())    
    if onScreen then
        SetTextScale(0.54, 0.54)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, Opacidade)
        SetTextDropshadow(0, 0, 0, 0, Opacidade)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)
    for i = 1, #Lugares do
      local source = source
      local Coordenadas = GetEntityCoords( GetPlayerPed(-1) )
      local Distancia = GetDistanceBetweenCoords(Coordenadas.x, Coordenadas.y, Coordenadas.z, Lugares[i]["X"], Lugares[i]["Y"], Lugares[i]["Z"], true)
      if Distancia < 5.0 then
        Opacidade = math.floor(200 - (Distancia * 25))
        Texto3D(Lugares[i]["X"], Lugares[i]["Y"], Lugares[i]["Z"], "PRESSIONE ~r~[E] ~w~PARA INTERAGIR.", Opacidade)
        if Distancia < 2 and IsControlJustPressed(1,38) and (GetEntityHealth(GetPlayerPed(-1)) > 100) then
          TriggerServerEvent("hospital:cobraae", source)
        end
        if Distancia < 2 and IsControlJustPressed(1,38) and (GetEntityHealth(GetPlayerPed(-1)) < 100) then
          TriggerServerEvent("hospital:shadowviado", source)
        end
        if IsControlJustPressed(1,38) and (GetEntityHealth(GetPlayerPed(-1)) == 200) then
          TriggerServerEvent("hospital:cheia", source)
        end
      end
    end
  end
end)

RegisterNetEvent("zezao:hospital")
AddEventHandler("zezao:hospital", function ()
  local jogador = GetPlayerPed(-1)
  SetEntityHealth(jogador, 200)
end)


Citizen.CreateThread(function()
  RequestModel(GetHashKey("s_m_m_doctor_01"))
  while not HasModelLoaded(GetHashKey("s_m_m_doctor_01")) do
    Wait(1)
  end

  local hospitalped = CreatePed(4, 0xd47303ac, 242.26,-1377.38,38.53, 45.404, false, true)
  SetEntityHeading(hospitalped, 320.404)
  FreezeEntityPosition(hospitalped, true)
  SetEntityInvincible(hospitalped, true)
  SetBlockingOfNonTemporaryEvents(hospitalped, true)
end)