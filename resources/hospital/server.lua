local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP")

RegisterServerEvent("hospital:cheia")
AddEventHandler("hospital:cheia", function()
	local source = source
	local user_id = vRP.getUserId(source)
  	TriggerClientEvent("chatMessage", source, "Hospital", {255, 0, 0}, "Você não precisa de tratamento.")
end)

RegisterServerEvent("hospital:cobraae")
AddEventHandler("hospital:cobraae", function()
	local source = source
	local user_id = vRP.getUserId(source)
    local preco = math.random(250,500)
  	if vRP.tryPayment(user_id,preco) then
  		TriggerClientEvent("zezao:hospital", source)
  		TriggerClientEvent("chatMessage", source, "Hospital", {255, 0, 0}, "Você pagou ^2" .. preco .. " ^7reais pelo tratamento.")
  	else
  		TriggerClientEvent("chatMessage", source, "Hospital", {255, 0, 0}, "Você não possui dinheiro suficiente.")
  	end
end)

RegisterServerEvent("hospital:shadowviado")
AddEventHandler("hospital:shadowviado", function()
	local source = source
	local user_id = vRP.getUserId(source)
    local preco = math.random(100,250)
  	if vRP.tryPayment(user_id,preco) then
  		TriggerClientEvent("zezao:hospital", source)
  		TriggerClientEvent("chatMessage", source, "Hospital", {255, 0, 0}, "Você pagou ^2" .. preco .. " ^7reais pelo tratamento.")
  	else
  		TriggerClientEvent("chatMessage", source, "Hospital", {255, 0, 0}, "Você não possui dinheiro suficiente.")
  	end
end)